<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    // Private function to verify data and return array with country and state
    private function  verifyNumbers($data){
        $newArray = [];
        foreach ($data as $customer){
            if(preg_match('/\(237\)\ ?[2368]\d{7,8}$/', $customer['phone'] )){
                $customer['country'] = 'Cameroon';
                $customer['state'] = 'OK';
            }elseif (preg_match('/\(251\)\ ?[1-59]\d{8}$/', $customer['phone'] )){
                $customer['country'] = 'Ethiopia';
                $customer['state'] = 'OK';
            }elseif (preg_match('/\(212\)\ ?[5-9]\d{8}$/', $customer['phone'] )){
                $customer['country'] = 'Morocco';
                $customer['state'] = 'OK';
            }elseif (preg_match('/\(258\)\ ?[28]\d{7,8}$/', $customer['phone'] )){
                $customer['country'] = 'Mozambique';
                $customer['state'] = 'OK';
            }elseif (preg_match('/\(256\)\ ?\d{9}$/', $customer['phone'] )){
                $customer['country'] = 'Uganda';
                $customer['state'] = 'OK';
            }else{
                $customer['country'] = '';
                $customer['state'] = 'NOK';
            }
            array_push($newArray, $customer);
        }
        return $newArray;
    }
    public function index(){
        $customers = Customer::get()->toArray();
        $response = $this->verifyNumbers($customers);
        return $response;
//        response()->json(, 200);
    }
}
