@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <h1 >Phone Numbers</h1>
                <table-phone/>
            </div>
        </div>
    </div>
@endsection
